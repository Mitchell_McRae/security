$ErrorActionPreference = "Continue"
<# 
.NOTES 
=========================================================================== 
General credits due, specific credits can be found in the code blocks.

Table formatting: https://poshoholic.com/2010/11/11/powershell-quick-tip-creating-wide-tables-with-powershell/

===========================================================================
Version 2.0 Mitchell McRae
Date: 27/09/2020
=========================================================================== 
Powershell modules required.
None
===========================================================================
.DESCRIPTION 
Script to audit Windows servers for common security vulnerabilities and export event logs that show intrusion attempts.

Secuirty Auditing Checks
Step S01. AD Account / Password Policy.
Step S02. Installed features and rolls.
Step S03. Members of default admin groups.
Step S04. Wild card search for RDS user group.
Step S05. Wild card search for VPN user group.
Step S06. Date, Time and Region settings.
Step S07. Export enabled ciphers list.
Step S08. User details for enabled AD users, txt + csv
Step S09. Detects SMBv1 and SMBv2 in Windows.
Step S10. Export Scheduled Tasks.
Step S11. Export Firewall Statuses.
Step S12. Windows updates.
Step S13. Routing and Remote Access.

Intrusion Detection Checks
Step E01. RAS failed logins and password policy. System 20271
Step E02. Counts AD failed login events a exports report. Security 4625
Step E03. Counts successful local RDP login events and exports report. This event ID only logs successful connections. 
            Microsoft-Windows-TerminalServices-RemoteConnectionManager/Operational 1149
Step E04. Lists failed logins against NPAS. Security 6273
Step E05. Counts SQL failed login events a exports report. Application 18456
Step E06. Counts Kerberos pre-authentication failures and exports report. Security 4771
Step E07. TBC Counts Remote Access failed login events. System 20271
#>

<#
# Runs the script as admin
# This can not be run in a function because it has to be running for the whole script.
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))
{
  # Relaunch as an elevated process:
  Start-Process powershell.exe "-File",('"{0}"' -f $MyInvocation.MyCommand.Path) -Verb RunAs
  exit
}#>

#Common Functions / Templates ###############################################################################################################

function YesOrNo {
    write-host "**********************************************" -ForegroundColor Red
    write-host "Please enter y/n" -ForegroundColor Gray
    write-host "**********************************************" -ForegroundColor Red
    write-host ""
}

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#Credit Due (Jon comments): https://richardspowershellblog.wordpress.com/2013/04/03/putting-the-date-in-a-file-name/
$InstanceDT = (Get-Date).ToString("yyyyMMdd HHmm")

$Line = '--------------------------------------------------------------------------------------------------------------'
$Blank = " "

#Report File
#New-Item "C:\WSAS Report $InstanceDT.txt" | Out-Null
$WSASReport = "C:\WSAS Report $InstanceDT.txt"
#Adding notes
Add-Content $WSASReport "Windows Security Audit Script Report" -Encoding ascii
Add-Content $WSASReport "" -Encoding ascii

#Read-Host "Code stop. Press any key to continue."

#Start of Code {{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}

#Step S01. AD Account / Password Policy.
function AccountPolicy {
    Write-Host "Step S01. Account Lockout Policy" -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Account Lockout Policy."

    if (Get-WindowsFeature -Name AD-Domain-Services | Where-Object InstallState -Eq Installed) {

        $Blank | Add-Content -Path $WSASReport
        Add-Content $WSASReport "AD Account / Password Policy"
        Get-ADDefaultDomainPasswordPolicy | Out-File -FilePath $WSASReport -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
               
    } Else {
 
        write-host "AD-Domain-Services role was not found." -ForegroundColor Yellow
        write-host ""
        Add-Content $WSASReport "AD-Domain-Services role was not found."
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    }
}

#Step S02. Installed features and rolls.
function RollsFeatures {
    Write-Host "Step S02. Getting installed features and rolls." -ForegroundColor Green
    Write-Host " "
    Write-Host "See WSAS report." -ForegroundColor Yellow
    Write-Host " "

    Add-Content $WSASReport "Installed Features"
    Get-WindowsFeature | Where-Object {$_.InstallState -eq 'Installed'} | Format-Table -Property name -AutoSize ` | Out-File -FilePath $WSASReport -Append -Encoding ascii
    $Blank, $Line, $Blank | Add-Content -Path $WSASReport
}

#Step S03. Members of default admin groups.
function ADAdminGroups {
    Write-Host "Step S03. Finding members of default admin groups." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Finding members of default admin groups." -Encoding ascii

    if (Get-WindowsFeature -Name AD-Domain-Services | Where-Object InstallState -Eq Installed) {
        
        Add-Content $WSASReport "Domain Admins" -Encoding ascii
        Get-AdGroupMember -identity "Domain Admins" | Select-Object name | Out-File -FilePath $WSASReport -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport

        Add-Content $WSASReport "Enterprise Admins" -Encoding ascii
        Get-AdGroupMember -identity "Enterprise Admins" | Select-Object name | Out-File -FilePath $WSASReport -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport

        Add-Content $WSASReport "Administrators Other" -Encoding ascii
        Get-AdGroupMember -identity "Administrators" | Select-Object name | Out-File -FilePath $WSASReport -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
        
    } Else {

        write-host "AD-Domain-Services role was not found." -ForegroundColor Yellow
        write-host ""
        Add-Content $WSASReport "AD-Domain-Services role was not found."
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    }
}

#Step S04. Wild card search for RDS user group.
function RDSGroups {
    Write-Host "Step S04. Finding members of RDS grouops." -ForegroundColor Green
    Write-Host " "

    $Blank | Add-Content -Path $WSASReport
    Add-Content $WSASReport "RDS Users"
    Add-Content $WSASReport "This is to only be used as a guide as it is a wild card search of AD"
    Add-Content $WSASReport "*RDS*,*RD Users*,*Remote Desktop*"

    if (Get-WindowsFeature -Name AD-Domain-Services | Where-Object InstallState -Eq Installed) {
        
        #Credit Due: https://www.reddit.com/r/PowerShell/comments/556zcj/getadgroup_filter_syntax_query/ 
        Get-ADGroup -Filter {name -like "*RDS*" -or name -like "*RD Users*" -or name -like "*Remote Desktop*"} | Get-ADGroupMember | Select-Object name | Out-File -FilePath $WSASReport -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
            
    } Else {
    
        write-host "AD-Domain-Services role was not found." -ForegroundColor Yellow
        write-host ""
        Add-Content $WSASReport "AD-Domain-Services role was not found."
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    }
}

#Step S05. Wild card search for VPN user group.
function VPNGroups {
    Write-Host "Step S05. Finding members of VPN grouops." -ForegroundColor Green
    Write-Host " "

    $Blank | Add-Content -Path $WSASReport
    Add-Content $WSASReport "VPN Users" -Encoding ascii
    Add-Content $WSASReport "This is to only be used as a guide as it is a group name search of AD"

    if (Get-WindowsFeature -Name AD-Domain-Services | Where-Object InstallState -Eq Installed) {
        
        Get-ADGroup -Filter {name -like "*VPN*,*VPN Users*,*AnyConnect*"} | Get-ADGroupMember | Select-Object name | Out-File -FilePath $WSASReport -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
            
    } Else {
    
        write-host "AD-Domain-Services role was not found." -ForegroundColor Yellow
        write-host ""
        Add-Content $WSASReport "AD-Domain-Services role was not found."
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    }
}

#Step S06. Date, Time and Region settings.
function NTPRegionSettings {
    Write-Host "Step S06. Searching for Date, Time and Region settings." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Gets Date, Time and Region settings."
    Write-Host " "

    #Checks the configured time zone.
    Add-Content $WSASReport "Configure time zone."
    w32tm /tz | Out-File -FilePath $WSASReport -Append -Encoding ascii
    $Blank | Add-Content -Path $WSASReport

    #Gets the configure time source.
    Add-Content $WSASReport "Configured time source."
    w32tm /query /source | Out-File -FilePath $WSASReport -Append -Encoding ascii
    $Blank | Add-Content -Path $WSASReport

    #Gets configured culture and region
    Add-Content $WSASReport "Configured culture/region setting."
    get-culture | format-list Displayname | Out-File -FilePath $WSASReport -Append -Encoding ascii
    $Blank, $Line, $Blank | Add-Content -Path $WSASReport
}

#Step S07. Export enabled ciphers list.
#Credit Due: (Comments Gungnir) https://community.spiceworks.com/topic/2033136-multiple-criteria-for-where-object
            
function EnabledCiphers {
    Write-Host "Step S07. Export enabled ciphers list." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Exporting enabled ciphers list."
    Write-Host " "

    #Checks for Server 2012/R2
    if ([System.Environment]::OSVersion.Version | Where-Object {($_.major -Match '[6]' -and $_.minor -Match '[2,3]')}) {
        
        Write-Host "Unfortunately I haven't been able to find a simple way of checking this in Server 2012, as Get-TLSCipherSuite isn't supported." -ForegroundColor Yellow
        Add-Content $WSASReport "Unfortunately I haven't been able to find a simple way of checking this in Server 2012, as Get-TLSCipherSuite isn't supported."
        Write-Host " "
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport

    #Checks for Server 2016 or newer.
    #Credit Due: https://blog.itpro.tv/where-object-powershell-command/
    }elseif ([System.Environment]::OSVersion.Version | Where-Object major -Match '[10]') {
        
        Get-TLSCipherSuite | Format-Table name,certificate,cipherlength | Out-File -FilePath $WSASReport -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    
    #Everything else.
    }else {
        Write-Host "Windows version is not compatabile." -ForegroundColor Green
    }
}

#Step S08. User details for enabled AD users.
#Credit Due: https://www.netwrix.com/how_to_determine_last_logon_date.html
#            https://social.technet.microsoft.com/Forums/ie/en-US/b6803035-002a-4e8d-8bee-1c1be45b9ae9/export-the-ad-users-list-with-created-date-and-last-login-date?forum=winserverDS
function UserDetails {
    Write-Host "Step S08. Getting user details for enabled AD users." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Getting user details for enabled AD users"

    if (Get-WindowsFeature -Name AD-Domain-Services | Where-Object InstallState -Eq Installed) {

        Add-Content $WSASReport "User details for enabled AD users, see report."

        $UD = "C:\WSAS User Details $InstanceDT.txt"
        $UDCSV = "C:\WSAS User Details $InstanceDT.csv" # TBC exporting to csv
        #Adding notes
        Add-Content $UD "Lists user details for all enabled AD users."
        Add-Content $UD "" -Encoding ascii        

        Get-ADUser -Filter {enabled -eq $true} -Properties * | Format-Table -Property name, UserPrincipalName, whenCreated, LastLogonDate -AutoSize ` | Out-File -FilePath $UD -Append -Encoding ascii
        Get-ADUser -Filter {enabled -eq $true} -Properties * | Select-Object -Property name, UserPrincipalName, whenCreated, LastLogonDate | Export-Csv -Path $UDCSV
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport

    } Else {

        write-host "AD-Domain-Services role was not found." -ForegroundColor Yellow
        write-host ""
        Add-Content $WSASReport "AD-Domain-Services role was not found."
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    }
}

#Step S09. Detects SMBv1 and SMBv2 in Windows.
function DetectSMBv {
    Write-Host "Step S09. Detects SMBv1 and SMBv2 in Windows." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Detecting SMBv1 and SMBv2."
    Write-Host " "

    Get-SmbServerConfiguration | Format-Table EnableSMB1Protocol,EnableSMB2Protocol | Out-File -FilePath $WSASReport -Append -Encoding ascii
    $Blank, $Line, $Blank | Add-Content -Path $WSASReport
}

#Step S10. Export Scheduled Tasks.
function ExportScheduledTasks {
    Write-Host "Step S10. Export Scheduled Tasks." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Exporting scheduled tasks."
    Add-Content $WSASReport "See report."
    Write-Host " "

    
    $ScheduledTasks = "C:\WSAS Exported Scheduled Tasks $InstanceDT.txt"
    Add-Content $ScheduledTasks "Export of Scheduled Tasks."
    Get-ScheduledTask -TaskPath "\" | Get-ScheduledTaskInfo |  Out-File -FilePath $ScheduledTasks -Append -Encoding ascii
    $Blank, $Line, $Blank | Add-Content -Path $WSASReport
}

#Step S11. Export Firewall Statuses.
function FirewallStatus {
    Write-Host "Step S11. Exporting Firewall Statuses." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Exporting firewall statuses."
    Write-Host " "

    Get-NetFirewallProfile -Name Public,domain,private | Format-Table -AutoSize ` -Property Name,Enabled | Out-File -FilePath $WSASReport -Append -Encoding ascii
    $Blank, $Line, $Blank | Add-Content -Path $WSASReport

}

#Step S12. Windows updates.
function WindowsUpdates {
    Write-Host "Step S12. Getting Windows update info." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Exporting Windows update info."
    Add-Content $WSASReport "If the below shows 1/01/1601 12:00:00 AM then the thing it's checking is null."
    Write-Host " "

    #Credit due: https://www.sysadmit.com/2019/03/windows-update-ver-fecha-powershell.html
    (New-Object -com "Microsoft.Update.AutoUpdate"). Results | Format-List | Out-File -FilePath $WSASReport -Append -Encoding ascii
    $Blank, $Line, $Blank | Add-Content -Path $WSASReport

}

#Step S13. Routing and Remote Access.
#The reason this is here and not below is that the RAS service is baked into Windows as a service, outside of the RAS role.
function RASService {
    Write-Host "Step S12. Checking Routing and Remote Access." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Exporting the service state of Routing and Remote Access."
    
    Get-Service -DisplayName "Routing and Remote Access" | Format-List -Property DisplayName, Name, Status, starttype | Out-File -FilePath $WSASReport -Append -Encoding ascii
    $Blank, $Line, $Blank | Add-Content -Path $WSASReport

}

#(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
#(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
#(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
#(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

#Step E01. RAS Account / Password Policy.
function RASFLE {
    Write-Host "Step E01. Searching for RAS failed login events" -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Counts RAS failed login events a exports report."

    
    function RASFailedLogins {
    Get-WinEvent "System" | 
    Where-Object{$_.ID -eq "20271"} | Select-Object -First 100000 | ForEach-Object{						
        New-Object PSObject -Property @{
            MachineName = $_.MachineName
            TimeCreated = $_.TimeCreated
            User = $_.Properties[1].Value                       
            SourceIP = $_.Properties[2].Value 
            }
        }
    } 

    if ((Get-WindowsFeature -Name RemoteAccess | Where-Object InstallState -Eq Installed) -and ((RASFailedLogins).count -gt 0)){

        #$CountRASFL = RASFailedLogins
        $RASFLR = "C:\WSAS RAS Failed Logins Report $InstanceDT.txt"
        #Adding notes
        Add-Content $RASFLR "If the date format is in US update the servers region settings and re run report."
        Add-Content $RASFLR "Event log: Security EventID: 20271"
        Add-Content $RASFLR ""        
        
        Add-Content $RASFLR "Event log count:"  
        (RASFailedLogins).count | Add-Content -Path $RASFLR, $WSASReport
        RASFailedLogins | Format-Table User,SourceIP,TimeCreated | Out-File -FilePath $RASFLR -Append -Encoding ascii

        $AccountLockoutPath = "HKLM:\SYSTEM\CurrentControlSet\Services\RemoteAccess\Parameters\AccountLockout"
        Get-ItemProperty -Path $AccountLockoutPath -Name "MaxDenials" | Format-Table PSChildName, MaxDenials | Out-File -FilePath $WSASReport -Append -Encoding ascii
        Get-ItemProperty -Path $AccountLockoutPath -Name "ResetTime (mins)" | Format-Table PSChildName, "ResetTime (mins)" | Out-File -FilePath $WSASReport -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport

    }elseif (Get-WindowsFeature -Name RemoteAccess | Where-Object InstallState -Eq Installed) {
        Add-Content $WSASReport "Event log: Security EventID: 20271"
        Add-Content $WSASReport "Routing and Remote Access is installed but no event logs have been found."
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
     
    } Else {

        write-host "RemoteAccess role was not found." -ForegroundColor Yellow
        write-host ""
        Add-Content $WSASReport "RemoteAccess role was not found."
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    }
}

#Step E02. Counts AD failed login events a exports report.
#Credit due: https://theposhwolf.com/howtos/Get-ADUserBadPasswords/
#            https://jdhitsolutions.com/blog/powershell/7193/better-event-logs-with-powershell/
function ADFailedLoginEvents {  
    Write-Host "Step E02. Searching for AD login events" -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Searching for AD login events."
   
    function qwer {  
        Get-WinEvent -FilterHashtable @{Logname ='security';ID =4625} | Select-Object -First 100000 | ForEach-Object{						
                    New-Object PSObject -Property @{
                        TimeCreated = $_.TimeCreated
                        User = $_.Properties[5].Value            
                        Domain = $_.Properties[6].Value            
                        Workstation = $_.Properties[13].Value 
                        IP = $_.Properties[19].Value 
                        SourcePort = $_.Properties[20].Value 
                    }
                }
            }

    $ADFLR = "C:\WSAS AD Failed Logins Report $InstanceDT.txt"
    #Adding notes
    Add-Content $ADFLR "If the date format is in US update the servers region settings and re run report."
    Add-Content $ADFLR "Event log: Security EventID: 4625"
    Add-Content $ADFLR ""

    #$Blank | Add-Content -Path $WSASReport
    Add-Content $WSASReport "Counts AD failed login attempts and exports a detailed list to a new test file: ADSAS Failed Login Report"
    Add-Content $ADFLR "Event log count:"
    (QWER).count | Add-Content -Path $WSASReport, $ADFLR
    qwer | Format-Table -Property * -AutoSize ` | Out-File -FilePath $ADFLR -Append -Encoding ascii
    $Blank, $Line, $Blank | Add-Content -Path $WSASReport
        
}

#Step E03. Counts successful local RDP login events and exports report. This event ID only logs successful connections.
function ServerLoginEvents {
    Write-Host "Step E03. Searching for successful local RDP login events." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Counts successful local RDP login events and exports report."

    function LocalServerLogins {
        Get-WinEvent "Microsoft-Windows-TerminalServices-RemoteConnectionManager/Operational" | 
        Where-Object{$_.ID -eq "1149"} | Select-Object -First 100000 | ForEach-Object{						
            New-Object PSObject -Property @{
                TimeCreated = $_.TimeCreated
                User = $_.Properties[0].Value            
                Domain = $_.Properties[1].Value            
                SourceIP = $_.Properties[2].Value 
            }
        }
    } 

    $RDPCR = "C:\WSAS RDP Connections Report $InstanceDT.txt"
    Add-Content $RDPCR "RDP Connections Report"
    Add-Content $RDPCR "This event ID only logs successful connections."
    Add-Content $RDPCR "Event log: Microsoft-Windows-TerminalServices-RemoteConnectionManager/Operational EventID: 1149"
    Add-Content $RDPCR ""
    Add-Content $RDPCR "Event log count:"
    (LocalServerLogins).count | Add-Content -Path $RDPCR, $WSASReport
    LocalServerLogins | Format-Table User,Domain,SourceIP,TimeCreated | Out-File -FilePath $RDPCR -Append -Encoding ascii
    $Blank, $Line, $Blank | Add-Content -Path $WSASReport
}

#Step E04. Lists failed logins against NPAS.
#Feature me please add csv output.
function NPASFLE {
    $Blank | Add-Content -Path $WSASReport
    Write-Host "Step E04. Lists failed logins against NPAS." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Lists failed logins against NPS."
      
    function hjkl {  
        Get-WinEvent -FilterHashtable @{Logname ='security';ID =6273} | Select-Object -First 100000 | ForEach-Object{						
                    New-Object PSObject -Property @{
                        TimeCreated = $_.TimeCreated
                        #User
                        User = $_.Properties[3].Value            
                        
                        #RADIUS Cleint
                        RadiusClientFriendlyName = $_.Properties[15].Value
                        RadiusClientIPAddress = $_.Properties[16].Value            
                        
                        #Authentication Details
                        ConnectionRequestPolicyName = $_.Properties[17].Value 
                        AuthenticationProvider = $_.Properties[19].Value 
                        AuthenticationServer = $_.Properties[20].Value
                        AuthenticationType = $_.Properties[21].Value
                        ReasonCode = $_.Properties[24].Value
                        Reason = $_.Properties[25].Value
                    }
                }
            }
            
    if ((Get-WindowsFeature -Name NPAS | Where-Object InstallState -Eq Installed) -and ((HJKL).count -gt 0)) {   
        
        $NPASFLR = "C:\WSAS NPAS Failed Login Report $InstanceDT.txt"
        #Adding notes
        Add-Content $NPASFLR "Lists failed logins against NPS."
        Add-Content $NPASFLR "Event log: Security EventID: 6273"
        Add-Content $NPASFLR ""

        (HJKL).count | Add-Content -Path $WSASReport, $NPASFLR
        Add-Content $NPASFLR "Event log count:"
        hjkl | Format-List -Property TimeCreated, User, RadiusClientFriendlyName, RadiusClientIPAddress, ConnectionRequestPolicyName, AuthenticationProvider, AuthenticationServer, AuthenticationType, ReasonCode, Reason | Out-File -FilePath $NPASFLR -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport

    } elseif (Get-WindowsFeature -Name NPAS | Where-Object InstallState -Eq Installed) {

        Add-Content $WSASReport "Event log: Security EventID: 6273"
        Add-Content $WSASReport "NPS is installed but no logs were found."
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    
    } Else {

        write-host "NPAS role was not found." -ForegroundColor Yellow
        write-host ""
        Add-Content $WSASReport "NPAS role was not found."
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    }
}

#Step E05. Counts SQL failed login events a exports report.
#Credit Due: https://thetechl33t.com/2015/09/09/powershell-script-to-check-if-sql-is-installed/
function SQLLoginEvents {
    $Blank | Add-Content -Path $WSASReport
    Write-Host "Step E05. Searching for SQL login events" -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Counts SQL failed login events a exports report."

    if (Test-Path "HKLM:\Software\Microsoft\Microsoft SQL Server\Instance Names\SQL") {
        function tyui {
            Get-WinEvent "Application" | 
            Where-Object{$_.ID -eq "18456"} | Select-Object -First 100000 |ForEach-Object{						
                New-Object PSObject -Property @{
                    MachineName = $_.MachineName
                    TimeCreated = $_.TimeCreated
                    User = $_.Properties[0].Value            
                    Domain = $_.Properties[1].Value            
                    SourceIP = $_.Properties[2].Value 
                }
            }
        }
        $SQLFLR = "C:\WSAS SQL Failed Login Report $InstanceDT.txt"
        #Adding notes
        Add-Content $SQLFLR "Lists failed logins to SQL DBs."
        Add-Content $SQLFLR "Event log: Application EventID: 18456"
        Add-Content $SQLFLR ""

        #$CountTYUI = tyui
        Add-Content $SQLFLR "Event log count:"
        (TYUI).count | Add-Content -Path $SQLFLR, $WSASReport
        tyui | Format-Table User,SourceIP,TimeCreated | Out-File -FilePath $SQLFLR -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport

    } Else {
        
        write-host "SQL was not found." -ForegroundColor Yellow
        write-host ""
        Add-Content $WSASReport "SQL was not found."
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    }
}

#Step E06. Counts Kerberos pre-authentication failures.
#Credit due: 
function KPAF {  
    Write-Host "Step E06. Searching for Kerberos pre-authentication failures" -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Kerberos pre-authentication failures." -Encoding ascii
    Write-Host "This will generate an error if no events are found, for some reason it is ignoring -ErrorAction SilentlyContinue" -ForegroundColor Yellow
   
    function zxcv {  
        Get-WinEvent -FilterHashtable @{Logname ='security';ID =4771} | Select-Object -First 100000 | ForEach-Object{						
                    New-Object PSObject -Property @{
                        TimeCreated = $_.TimeCreated
                        User = $_.Properties[0].Value            
                        ServiceName = $_.Properties[2].Value            
                        IP = $_.Properties[6].Value 
                        SourcePort = $_.Properties[7].Value 
                    }
                }
            }

    if ((zxcv).count -gt 0) {
        
        $KPAFLR = "C:\WSAS Kerberos Failed Logins Report $InstanceDT.txt"
        #Adding notes
        Add-Content $KPAFLR "If the date format is in US update the servers region settings and re run report."
        Add-Content $KPAFLR "Event log: Security EventID: 4771"
        Add-Content $KPAFLR ""

        $Blank | Add-Content -Path $WSASReport
        Add-Content $WSASReport "Counts Kerberos failed login attempts and exports a detailed list to a new test file: WSAS Failed Login Report"
        Add-Content $KPAFLR "Event log count:"
        (ZXCV).count | Add-Content -Path $WSASReport, $KPAFLR
        zxcv | Format-Table -Property TimeCreated,User,IP,SourcePort,ServiceName | Out-File -FilePath $KPAFLR -Append -Encoding ascii
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport

    }else {

        Add-Content $WSASReport "Event log: Security EventID: 4771"
        Add-Content $WSASReport "No event logs were found."
        Write-Host "No event logs were found." -ForegroundColor Yellow
        Write-Host ""
        $Blank, $Line, $Blank | Add-Content -Path $WSASReport
    }
}

#Step E07. TBC Event ID 20187.
#Credit due: 
function RAFL {  
    Write-Host "Step E07. Counts Remote Access failed login events." -ForegroundColor Green
    Write-Host " "
    Add-Content $WSASReport "Remote Access failed login events."
    function uiop {  
        Get-WinEvent -FilterHashtable @{Logname ='system';ID = 20271} | Select-Object -First 100000 | ForEach-Object{						
                    New-Object PSObject -Property @{
                        TimeCreated = $_.TimeCreated
                        User = $_.Properties[1].Value            
                        IP = $_.Properties[2].Value 
                    }
                }
            }

    $RAFLR = "C:\WSAS Remote Access Failed Logins Report $InstanceDT.txt"
    #Adding notes
    Add-Content $RAFLR "If the date format is in US update the servers region settings and re run report."
    Add-Content $RAFLR "Event log: System EventID: 20271"
    Add-Content $RAFLR ""

    $Blank | Add-Content -Path $WSASReport
    Add-Content $WSASReport "Counts Remote Access failed login attempts and exports a detailed list to a new test file: WSAS Failed Login Report" -Encoding ascii
    (uiop).count | Add-Content -Path $WSASReport
    uiop | Format-Table -Property TimeCreated,User,IP | Out-File -FilePath $RAFLR -Append -Encoding ascii
    $Blank, $Line, $Blank | Add-Content -Path $WSASReport
}

#Secuirty Auditing Checks
AccountPolicy
RollsFeatures
ADAdminGroups
RDSGroups
VPNGroups
NTPRegionSettings
EnabledCiphers
UserDetails
DetectSMBv
ExportScheduledTasks
FirewallStatus
WindowsUpdates
RASService

#Intrusion Detection Checks
RASFLE
ADFailedLoginEvents
ServerLoginEvents
NPASFLE
SQLLoginEvents
KPAF

Write-Host "Windows Secuirty Auditing Script.
Files are created in C:\WSAS... yyyymmdd hhmm.txt
" -ForegroundColor DarkGray

Read-Host "The script has finished"