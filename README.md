# Windows Security Auditing Script.
 
Can be used either as preventative checks or post hack investigations.
 
Creates reports based on roles and programs installed on the server.
 
Reports are exported to C:\WSAS... (date/time the file was run).txt
 
## Compatibility ###
 
#### Server 2008R2, SBS 2011 and older.
 
They are EOL and also advancements in Powershell have left them behind.
 
#### Server 2012 
 
Some servers have issues generating the event viewer reports, the process just hangs. 

Unfortunately I haven't found an easy to export the enabled ciphers in Server 2012, as Get-TLSCipherSuite isn’t supported.
 
#### Server 2016 and newer.
 
Compatible without issues.
 
## Secuirty Auditing Checks ##
 
#### Step S01. AD Account / Password Policy.
Exports the AD Account / Password Policy
 
Requirements: role AD-Domain-Services  
Counter: No  
Report Name: WSAS Report  

#### Step S02. Installed features and rolls.
Exports a list of all installed roles.  
 
Requirements: none  
Counter: No  
Report Name: WSAS Report 

#### Step S03. Members of default admin groups.
Export the members of Domain Admins, Enterprise Admins and any other members of admin groups.  
 
Requirements: role AD-Domain-Services  
Counter: No  
Report Name: WSAS Report  
 
#### Step S04. Wild card search for RDS user group.
Does a wild card search for RDS groups and export the members.  
 
Filters: RDS, RD Users, Remote Desktop   
 
Requirements: role AD-Domain-Services  
Counter: No  
Report Name: WSAS Report  
 
#### Step S05. Wild card search for VPN user group.
Does a wild card search for VPN groups and exports the members.  
 
Filters: VPN   
 
Requirements: role AD-Domain-Services  
Counter: No  
Report Name: WSAS Report 

#### Step S06. Date, Time and Region settings.
Exports the Date, Time and Region settings.    
 
Requirements: None  
Counter: No  
Report Name: WSAS Report 

#### Step S07. Export enabled ciphers list.
Exports the list of all enabled ciphers.    
 
Requirements: None  
Counter: No  
Report Name: WSAS Report 

#### Step S08. Detects SMBv1 and SMBv2 in Windows. 
Detects SMBv1 and SMBv2 states in Windows.    
 
Requirements: None  
Counter: No  
Report Name: WSAS Report 

#### Step S09. User details for enabled AD users.
Exports the last login date and creation date for all enabled users in AD.
 
Requirements: role AD-Domain-Services  
Counter: No  
Report Name: WSAS Last Login Date  

#### Step S10. Export Scheduled Tasks.
Exports scheduled task in the root "\" folder.  
 
Requirements: None  
Counter: No  
Report Name: WSAS Exported Scheduled Tasks  

#### Step S11. Export Firewall Statuses.
Exports the statuses of the Windows firewall.  
 
Requirements: None  
Counter: No  
Report Name: WSAS Exported Scheduled Tasks  

#### Step S12. Windows updates.
Exports the status of Windows updates.  
 
Requirements: None  
Counter: No  
Report Name: WSAS Exported Scheduled Tasks  

#### Step S13. Routing and Remote Access service.
Exports the status of the Routing and Remote Access service.  
 
Requirements: None  
Counter: No  
Report Name: WSAS Exported Scheduled Tasks

 
## Intrusion Detection Checks ##

The below are limited to the newest 100,000 events, though this can still take a while to generate.

#### Step E01. RAS Account / Password Policy.
Exports the RAS account policy reg key. 
Reg key location: HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\RemoteAccess\Parameters\AccountLockout  
 
Microsoft KB: https://support.microsoft.com/en-au/help/816118/how-to-configure-remote-access-client-account-lockout-in-windows-serve  
 
Requirements: role RemoteAccess  
Counter: No  
Report Name: WSAS Report  
  
#### Step E02. Counts AD failed login events and exports report.
Searches the *Security* event log for EventID *4625* and exports entries to the report.
 
Fields Exported:  
 
Requirements: role AD-Domain-Services  
Counter: Yes  
Report Name: WSAS AD Failed Logins Report $InstanceDT.txt
 
#### Step E03. Counts local server failed login events and exports report.
Searches the *Microsoft-Windows-TerminalServices-RemoteConnectionManager/Operational* event log for EventID *1149* and exports entries to the report.  
 
Fields Exported: User, Domain, SourceIP, TimeCreated  
 
Requirements: none  
Counter: Yes  
Report Name: WSAS RDP Connections Report $InstanceDT.txt"  
 
#### Step E04. Lists failed logins against NPS.
Searches the *Security* event log for EventID *6273* and exports entries to the report.
 
Requirements: role NPAS  
Counter: Yes  
Report Name: WSAS NPAS Failed Login Report $InstanceDT.txt
 
#### Step E05. Counts SQL failed login events and exports report.
Searches the *Application* event log for EventID *18456* and exports entries to the report.
 
Requirements: HKLM:\Software\Microsoft\Microsoft SQL Server\Instance Names\SQL
Counter: Yes  
Report Name: WSAS SQL Failed Login Report $InstanceDT.txt

#### Step E06. Counts Kerberos pre-authentication failures.
Searches the *Security* event log for EventID *4771* and exports entries to the report.
 
Requirements: None
Counter: Yes  
Report Name: WSAS Kerberos Failed Logins Report $InstanceDT.txt
 
 
### How do I get set up? ###
 
Download onto the server and run as admin.
 
### Requirements ###
 
* Being able to write files to C:
